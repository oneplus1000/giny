package giny

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
)

func StringUnmarshalBody(r *http.Request) (string, error) {
	data, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return "", err
	}
	return string(data), nil
}

func JsonUnmarshalBody(r *http.Request, obj interface{}) error {

	data, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return err
	}

	err = json.Unmarshal(data, &obj)
	if err != nil {
		return err
	}
	return nil
}

/*
var respformat = "{ \"data\" : %s ,  \"errmsg\" : \"%s\", \"errcode\" : %d   }"

//write json data to http respons
func WriteJsonDataWithCtx(ctx *gin.Context, data interface{}) {
	WriteJsonData(ctx.Writer, ctx.Request, data)
}

//write json data to http response
func WriteJsonData(w http.ResponseWriter, r *http.Request, data interface{}) {
	b, err := json.Marshal(data)
	if err != nil {
		w.WriteHeader(500)
		return
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	fmt.Fprintf(w, respformat, string(b), "", 0)
}

//write json error to http response
func WriteJsonErrWithCtx(ctx *gin.Context, err error, errcode int) {
	WriteJsonErr(ctx.Writer, ctx.Request, err, errcode)
}

//write json error to http response
func WriteJsonErr(w http.ResponseWriter, r *http.Request, err error, errcode int) {
	msg := ""
	if err != nil {
		msg = err.Error()
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	//w.WriteHeader(errcode)
	fmt.Fprintf(w, respformat, "null", msg, errcode)
}
*/
