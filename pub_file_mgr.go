package giny

import (
	"bytes"
	"errors"
	"fmt"
	"html/template"

	"github.com/gin-gonic/gin"
)

type PubFileMgr struct {
	pubPath       string
	pubVirPath    string
	scriptVersion string
	jsfiles       map[string]([]string)
	cssfiles      map[string]([]string)
}

var ErrDupJsName = errors.New("dup JsBundle name")
var ErrDupCssName = errors.New("dup CssBundle name")

func NewPubFileMgr(pubPath string, pubVirPath string, scriptVersion string) *PubFileMgr {
	var pubMgr PubFileMgr
	pubMgr.pubPath = pubPath
	pubMgr.pubVirPath = pubVirPath
	pubMgr.scriptVersion = scriptVersion
	pubMgr.jsfiles = make(map[string]([]string))
	pubMgr.cssfiles = make(map[string]([]string))
	return &pubMgr
}

func (p *PubFileMgr) RegistJs(name string, files ...string) error {
	if _, ok := p.jsfiles[name]; ok {
		return ErrDupJsName
	}
	p.jsfiles[name] = files
	return nil
}

func (p *PubFileMgr) RegistCss(name string, files ...string) error {
	if _, ok := p.cssfiles[name]; ok {
		return ErrDupCssName
	}
	p.cssfiles[name] = files
	return nil
}

func (p *PubFileMgr) JsBundle(data interface{}, name string) template.HTML {
	if files, ok := p.jsfiles[name]; ok {
		var buff bytes.Buffer
		buff.WriteString(fmt.Sprintf("<!-- JsBundle %s -->\n", name))
		for _, file := range files {
			buff.WriteString(fmt.Sprintf("<script src=\"/%s/%s\" ></script>\n", p.pubVirPath, file))
		}
		return template.HTML(buff.String())
	}
	return template.HTML(fmt.Sprintf("<!-- Error:JsBundle %s not found  -->\n", name))
}

func (p *PubFileMgr) CssBundle(data interface{}, name string) template.HTML {
	if files, ok := p.cssfiles[name]; ok {
		var buff bytes.Buffer
		buff.WriteString(fmt.Sprintf("<!-- CssBundle %s -->\n", name))
		for _, file := range files {
			buff.WriteString(fmt.Sprintf("<link rel=\"stylesheet\"  href=\"/%s/%s\" />\n", p.pubVirPath, file))
		}
		return template.HTML(buff.String())
	}
	return template.HTML(fmt.Sprintf("<!-- Error:CssBundle %s not found  -->\n", name))
}

func (p *PubFileMgr) Static(router *gin.Engine) {
	router.Static(p.pubVirPath, p.pubPath)
}
